package com.pk.studentdemo.controller;

import com.pk.studentdemo.ResponseJsonUtil;
import com.pk.studentdemo.dto.StudentClassOnly;
import com.pk.studentdemo.dto.StudentDto;
import com.pk.studentdemo.model.Student;
import com.pk.studentdemo.repository.StudentRepository;
import com.pk.studentdemo.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/students")
@Slf4j
public class StudentController {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Autowired
    StudentService studentService;
    @Autowired
    StudentRepository studentRepository;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createStudent(@RequestBody StudentDto studentDto) {

        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(this.studentService.createStudent(studentDto)), HttpStatus.CREATED);

    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getStudent(@PathVariable Long id) {

        log.info("Inside getStudent method ");
        Student student = studentRepository.findStudentById(id);

        if (student == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Optional<Student> result = studentRepository.findById(id);

           return new ResponseEntity(ResponseJsonUtil.getSuccessResponseJson(result), HttpStatus.OK);

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteStudent(@PathVariable Long id) {

        log.info("Inside deleteStudent method ");
        Student student = studentRepository.findStudentById(id);
        if (student == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        student.setActive(false);

        Student inactiveStudent = studentRepository.save(student);
        return new ResponseEntity(ResponseJsonUtil.getSuccessResponseJson(inactiveStudent), HttpStatus.OK);

    }

    @PatchMapping(value = "{/id}")
    public ResponseEntity updateStudentClass(@RequestBody StudentClassOnly student, @PathVariable Long id) {
        log.info("Inside Patch update Student class  method ");

        Student updateStudent = studentRepository.findStudentById(id);
        if (updateStudent == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        updateStudent.setSClass(student.getStudentClass());
        studentRepository.save(updateStudent);
        return new ResponseEntity(ResponseJsonUtil.getSuccessResponseJson("updated successfully"), HttpStatus.OK);


    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getStudents(@RequestParam(value = "classes", required = false) String[] arr,
                                              @RequestParam(value = "active", required = false, defaultValue = "true") boolean active,
                                              @RequestParam(value = "admissionYearAfter", required = false) Integer admissionYearAfter,
                                              @RequestParam(value = "admissionYearBefore", required = false) Integer admissionYearBefore,
                                              @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
                                              @RequestParam(value = "pageSize", defaultValue = "20", required = false) int pageSize) {


        log.info("Inside getStudents() :");

        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Session session = sessionFactory.openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Student> query = cb.createQuery(Student.class);

        Root<Student> root = query.from(Student.class);
        List<Predicate> predicates = new ArrayList<Predicate>();

        if (arr!=null) {

            predicates.add(root.get("sClass").in(arr));
        }
        predicates.add(cb.equal(root.get("active"), active));

        if(admissionYearAfter !=null) {
            predicates.add(cb.greaterThanOrEqualTo(root.get("admissionYear"), admissionYearAfter));
        }

        if(admissionYearBefore !=null) {
            predicates.add(cb.lessThanOrEqualTo(root.get("admissionYear"), admissionYearBefore));
        }


        if (predicates.size() > 0) {
            query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
        }else {
            query.select(root);
           }

        TypedQuery<Student> q = session.createQuery(query);
        Pageable pageRequest = PageRequest.of(pageNumber, pageSize);
        final int currentTotal = (int) pageRequest.getOffset() + pageRequest.getPageSize();

        log.info("Page request : " + pageRequest + "Current total : " + currentTotal + "Page size :" + pageRequest.getPageSize());

        q.setFirstResult((int)pageRequest.getOffset());
        q.setMaxResults(pageRequest.getPageSize());

        Page<Student> result = new PageImpl<>(q.getResultList(), pageRequest, currentTotal);
        List<Student> studentList = result.getContent();

        log.info("List size " + studentList.size() + "Student List " + studentList);

        if (studentList.size() > 0) {

            return new ResponseEntity(ResponseJsonUtil.getSuccessResponseJson(studentList), HttpStatus.OK);
        } else {
            return new ResponseEntity(ResponseJsonUtil.getSuccessResponseJson("Empty result"), HttpStatus.OK);
        }
    }


}
