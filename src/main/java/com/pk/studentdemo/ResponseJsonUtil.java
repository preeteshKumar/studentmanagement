package com.pk.studentdemo;

import java.util.HashMap;
import java.util.Map;

public class ResponseJsonUtil {

    public static Map<String, String> getSuccessResponseJson() {
        Map<String, String> responseBuilder = new HashMap<>();
        responseBuilder.put(ResponseConstants.STATUS, ResponseConstants.SUCCESS);
        return responseBuilder;
    }

    public static Map<String, Object> getSuccessResponseJson(Object data) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put(ResponseConstants.STATUS, ResponseConstants.SUCCESS);
        responseBuilder.put(ResponseConstants.DATA, data);
        return responseBuilder;
    }
}
