package com.pk.studentdemo.service;

import com.pk.studentdemo.dto.StudentDto;
import com.pk.studentdemo.model.Student;
import com.pk.studentdemo.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Service
@Slf4j
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public boolean createStudent(StudentDto studentDto){

        log.info("Inside createStudent() method" + studentDto);

        if(studentDto == null)return false;

             try {
            Student student = new Student();
            student.setAdmissionYear(studentDto.getAdmissionDate());
            student.setName(studentDto.getName());
            student.setSClass(studentDto.getStudentClass());

         //   log.info(student.getName()+ student.getSClass());

           Student s =  this.studentRepository.save(student);
           log.info("student details " + s);

        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

        return true;

    }

}
