package com.pk.studentdemo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Table(name = "student" )
public class Student implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name",nullable = false,updatable = false)
    private String name;

    @Column(name = "class",nullable = false)
    private int sClass;

    @Column(name = "active",nullable = false)
    private boolean active = true;

    @Column(name = "admission_year", nullable = false,updatable = false)
    private Integer admissionYear ;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sClass=" + sClass +
                ", active=" + active +
                ", admissionYear=" + admissionYear +
                '}';
    }
}
